package com.galvanize;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ZipCodeProcessorTest {

    // write your tests here
    @Test
    public void processShouldAcceptValidInput() throws NoServiceException, InvalidFormatException {
        //-----SETUP-----
        Verifier myVerifier = new Verifier();
        ZipCodeProcessor myZipCodeProcessor = new ZipCodeProcessor(myVerifier);
    
        //-----ENACT-----
        String result = myZipCodeProcessor.process("80302");
    
        //-----ASSERT-----
        assertEquals("Thank you!  Your package will arrive soon.", result);
        //-----TEARDOWN-----
    }
    @Test
    public void processShouldThrowInvalidFormatExceptionIfInputIsInvalid() throws NoServiceException, InvalidFormatException {
        //-----SETUP-----
        Verifier myVerifier = new Verifier();
        ZipCodeProcessor myZipCodeProcessor = new ZipCodeProcessor(myVerifier);

        //-----ENACT-----
        String result = myZipCodeProcessor.process("2345678");
        String result2 = myZipCodeProcessor.process("321");

        //-----ASSERT-----
        assertEquals("The zip code you entered was the wrong length.", result);
        assertEquals("The zip code you entered was the wrong length.", result2);
        //-----TEARDOWN-----
    }
    @Test
    public void processShouldThrowNoServiceExceptionIfInputIsValidButBeginsWith1() throws NoServiceException, InvalidFormatException {
        //-----SETUP-----
        Verifier myVerifier = new Verifier();
        ZipCodeProcessor myZipCodeProcessor = new ZipCodeProcessor(myVerifier);

        //-----ENACT-----
        String result = myZipCodeProcessor.process("12234");

        //-----ASSERT-----
        assertEquals("We're sorry, but the zip code you entered is out of our range.", result);
        //-----TEARDOWN-----
    }
    @Test
    public void assertTrue() {
        assertEquals(true, true);
    }

}