package com.galvanize;

public class InputTooShortException extends Throwable {
    public InputTooShortException(String s) {
        super(s);
    }
}
