package com.galvanize;

public class ZipCodeProcessor {

    // don't alter this code...
    private final Verifier verifier;

    public ZipCodeProcessor(Verifier verifier) {
        this.verifier = verifier;
    }

    public String process(String zip){
        String result = "";

        try{
            verifier.verify(zip);
            result = "Thank you!  Your package will arrive soon.";

        }catch(NoServiceException e){
            result = "We're sorry, but the zip code you entered is out of our range.";

        }catch(InvalidFormatException e){
            result = "The zip code you entered was the wrong length.";

        }

        return result;
    }

}
