package com.galvanize;

public class NoServiceException extends Exception {
    public NoServiceException(String s) {
        super(s);
    }
}
